// console.log("Hello 204!");

// Problem 1

let number = Number(prompt("Give me a number:")); {
	console.log("The number you provided is " + number);
}

for (let count = number ; count >= 0 ; count --)
	{
	
		if (count % 10 === 0) 
			{
			console.log ("This number is divisible by 10. Skipping the number."); 
			continue;	
			}

		if (count % 5 === 0) 
			{
			console.log (count)
			continue;
			}

		if (count <= 50 ) 
			{
			console.log ("The current value is at 50, Terminating the loop");
			break;
			}
	}

// Problem 2

let word = "supercalifragilisticexpialidocious";

let consonants = "";

	for (let i = 0; i < word.length; i++)
	{
	if (
		word[i].toLowerCase() === "a" ||
		word[i].toLowerCase() === "e" ||
		word[i].toLowerCase() === "i" ||
		word[i].toLowerCase() === "o" ||
		word[i].toLowerCase() === "u" 
		)
		{
		continue;
		}
		else
		{
		consonants = consonants + word[i];
		}
	}; 

console.log (word);
console.log(consonants);

